function [  ] = getTrajectory2( gyro, acce, q )

g = [0,0,-9.821];
dt = 1/120;

acceleration = zeros(length(acce),3);
velocity = zeros(length(acce),3);
position = zeros(length(acce),3);

midStance = findMidStance(gyro,acce,dt,g);

for i = 2:length(acce)
    acceleration(i-1,:) = (QuatToRM(q(i-1,:))*acce(i-1,:)')'+g;
    velocity(i,:) = velocity(i-1,:) + acceleration(i-1,:)*dt;
    position(i,:) = position(i-1,:) + velocity(i-1,:)*dt + 0.5*acceleration(i-1,:)*dt^2;
    
    % velocity correction
    k = find(midStance == i,1);
    if ~isempty(k)
        if k > 1
            prei = midStance(k-1);
            EA = velocity(i,:)/(dt*(i-prei));
            for j = prei+1:i
                position(j,:) = position(j,:)-0.5*EA*(dt*(j-prei))^2;
                velocity(j,:) = velocity(j,:)-EA*(dt*(j-prei));
            end
        end
    end
end

figure;
plot3(position(:,1),position(:,2),position(:,3));
axis equal; grid on;

end


function [ PKF ] = covProp( PKF, QKF, E, gyro, dt )

WB(1,1:3) = [1, sin(E(1))*tan(E(2)), cos(E(1))*tan(E(2))];
WB(2,1:3) = [0, cos(E(1)), -sin(E(1))];
WB(3,1:3) = [0, sin(E(1))/cos(E(2)), cos(E(1))/cos(E(2))];
VB(1,1) = cos(E(1))*tan(E(2))*gyro(2) - sin(E(1))*tan(E(2))*gyro(3);
VB(1,2) = sin(E(1))*cos(E(2))^-2*gyro(2) + cos(E(1))*cos(E(2))^-2*gyro(3);
VB(1,3) = 0;
VB(2,1) = -sin(E(1))*gyro(2)-cos(E(1))*gyro(3);
VB(2,2) = 0;
VB(2,3) = 0;
VB(3,1) = cos(E(1))/cos(E(2))*gyro(2) - sin(E(1))/cos(E(2))*gyro(3);
VB(3,2) = sin(E(1))*sin(E(2))*cos(E(2))^-2*gyro(2) + cos(E(1))*sin(E(2))*cos(E(2))^-2*gyro(3);
VB(3,3) = 0;
F = expm(VB*dt);

PKF = F*PKF*F' + WB*QKF*WB';

end


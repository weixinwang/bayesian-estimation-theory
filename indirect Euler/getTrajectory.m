function [ position ] = getTrajectory(gyro,acce,IMU,obs,KF)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('IMU','var')
    IMU = 'MPU';
end
if ~exist('obs','var')
    obs = 'eul';
end
if strcmp(obs,'grav') && ~exist('KF','var')
    KF = 'EKF';
end
if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    addpath('..\Gaussian weighted integral');
end

% Constants
g = [0;0;-9.821];
grav = norm(g);
gyroThresholdVelocity = 0.8;
acceThreshold = 0.8;

% state variables
P = zeros(3,1);
V = zeros(3,1);
E = qua2eul(getInitQuat(acce(1,:)))';

% kalman filter parameters
conf = getMeaConf();
PKF = conf.euler^2*eye(3);
if strcmp(IMU,'MPU')
    [dt,QG] = getIMU_MPU();
elseif strcmp(IMU,'YOS')
    [dt,QG] = getIMU_YOST();
elseif strcmp(IMU,'XSE')
    [dt,QG] = getIMU_XSENS();
elseif strcmp(IMU,'MEM')
    [dt,QG] = getIMU_MEM();
end
QKF = QG*eye(3);
if strcmp(obs,'grav')
    RKF = conf.gravity^2*eye(3);
elseif strcmp(obs,'eul')
    RKF = conf.euler^2*eye(3);
end

% Monitors
euler = zeros(size(gyro,1),3);
acceleration = zeros(size(gyro,1),3);
velocity = zeros(size(gyro,1),3);
position = zeros(size(gyro,1),3);

% pre-analysis
applyKFZUPT = sqrt(sum(gyro.^2,2))<gyroThresholdVelocity & abs(sqrt(sum(acce.^2,2))-grav)<acceThreshold;
addpath('..');
midStance = findMidStance(gyro,acce,dt,g);
rmpath('..');

obsEul = strcmp(obs,'eul');
for i = 2:1:size(gyro,1)
    % forward integration
    PKF = covProp(PKF,QKF,E,gyro(i-1,:),dt);
    [P,V,E,A] = eulInt(acce(i,:)',gyro(i,:)',acce(i-1,:)',gyro(i-1,:)',P,V,E,dt,g);
    
    % ESKF
    if applyKFZUPT(i)
        if obsEul     % convert Grav to Euler angle as observation
            EG = qua2eul(getInitQuat(acce(i,:)))';
            Error = wrapToPi(EG-E);
            K = PKF*(PKF+RKF)^-1;
            E = [E(1:2)+K(1:2,1:2)*Error(1:2);E(3)];
            PKF = (eye(3)-K)*PKF;
        else          % observe Euler angle directly
            if strcmp(KF,'EKF')     % extended Kalman filter
                Error = acce(i,:)'./norm(acce(i,:))-eul2rm(E)'*[0;0;1];
                H = [0 -cos(E(2)) 0
                    cos(E(1))*cos(E(2)) -sin(E(1))*sin(E(2)) 0
                    -sin(E(1))*cos(E(2)) -cos(E(1))*sin(E(2)) 0];
                K = PKF*H'*(H*PKF*H'+RKF)^-1;
                E = E+K*Error;
                PKF = (eye(3)-K*H)*PKF;
            elseif strcmp(KF,'UKF')  % Uncented Kalman filter
                [w,sig] = generateSigmaPoint(E,PKF,'Unscented');
                gravSig = zeros(3,length(w));
                Pzz = zeros(3); Pxz = zeros(3);
                for j = 1:length(w)
                    gravSig(:,j) = [-sin(sig(2,j)); sin(sig(1,j))*cos(sig(2,j)); cos(sig(1,j))*cos(sig(2,j))];
                    Pzz = Pzz+w(j)*gravSig(:,j)*gravSig(:,j)';
                    Pxz = Pxz+w(j)*sig(:,j)*gravSig(:,j)';
                end
                QG = gravSig*w';
                Pzz = Pzz-QG*QG'+RKF;
                Pxz = Pxz-E*QG';
                K = Pxz*Pzz^-1;
                E = E+K*(acce(i,:)'./norm(acce(i,:))-QG);
                PKF = PKF-K*Pzz*K';
            end
        end
    end
    
    % velocity correction
    k = find(midStance == i,1);
    if ~isempty(k)
        if k > 1
            prei = midStance(k-1);
            EA = V/(dt*(i-prei));
            for j = prei+1:i
                position(j,:) = position(j,:)-0.5*EA'*(dt*(j-prei))^2;
                velocity(j,:) = velocity(j,:)-EA'*(dt*(j-prei));
            end
            P = P - 0.5*EA*(dt*(i-prei))^2;
        end
        V = [0;0;0];
    end
    
    euler(i,:) = E;
    acceleration(i,:) = A;
    velocity(i,:) = V;
    position(i,:) = P;
end

figure;
plot3(position(:,1),position(:,2),position(:,3));
axis equal; grid on;

if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    rmpath('..\Gaussian weighted integral');
end

end


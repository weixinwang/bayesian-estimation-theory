function [ ] = compare(f, miu, sigma, dim1, dim2)
% compare performance of numerical evaluation of Gaussian weighted integral
% f: transform function, nx-by-1 function handle, single input variable "x"
%    which is a nx-by-1 vector. e.g. f = @(x)[x(1)^2;x(1)*x(2)].
% miu: mean of distribution, nx-by-1 vector.
% sigma: covariance matrix of distribution, nx-by-nx matrix
% dim1, dim2: dimension of error ellipse 
close all;

% default distribution and transform function
if ~exist('f','var') && ~exist('miu','var') && ~exist('sigma','var')
    miu = [0;1];
    sigma = [1,0.5;0.5,1];
    f = @(x)[exp(x(1))+x(2); x(1)*x(2)];
end

if ~exist('dim1','var') && ~exist('dim2','var')
    dim1 = 1;
    dim2 = 2;
end

% check input variable
if size(miu,2) ~= 1
    error('"miu" must be a column vector');
end
fstr = func2str(f);
if contains(fstr,',')
    error('"f" must be a column function');
end

% dimensions
nx = length(miu);

% convert function handle to symbolic function
xcat = '';
for i = 1:nx
    fstr = replace(fstr,strcat('x(',num2str(i),')'),strcat('x',num2str(i)));
    xcat = strcat(xcat,'x',num2str(i),',');
end
xcat = xcat(1:end-1);
fstr = replace(fstr,'@(x)',strcat('@(',xcat,')'));
fsym = sym(str2func(fstr));

%% evaluate different numerical integration method
% first order extended
[ymiuExtended1, ysigmaExtended1] = integrate(fsym,miu,sigma,'Extended1');

% second order extended
[ymiuExtended2, ysigmaExtended2] = integrate(fsym,miu,sigma,'Extended2');

% unscented
[ymiuUnscented, ysigmaUnscented] = integrate(f,miu,sigma,'Unscented');

% spherical simplex
[ymiuSpherical, ysigmaSpherical] = integrate(f,miu,sigma,'Spherical');

% gauss-hermite
[ymiuHermite, ysigmaHermite] = integrate(f,miu,sigma,'Hermite');

% monte carlo
[ymiuMonteCarlo, ysigmaMonteCarlo] = integrate(f,miu,sigma,'Monte carlo');

%% monte carlo simulation
x = mvnrnd(miu',sigma,10000)';
y = zeros(nx,10000);
for i = 1:10000
    y(:,i) = f(x(:,i));
end
figure(1); hold on;
scatter(y(dim1,:),y(dim2,:),6); axis equal;

%% plot error ellipse of the first 2 dimensions
phi = 0:0.01:2*pi;
x = [3*cos(phi);3*sin(phi)];

% first order extended
D = cholcov(ysigmaExtended1([dim1,dim2],[dim1,dim2]));
y = ymiuExtended1([dim1,dim2]) + D*x;
plot(y(1,:),y(2,:));

% second order extended
D = cholcov(ysigmaExtended2([dim1,dim2],[dim1,dim2]));
y = ymiuExtended2([dim1,dim2]) + D*x;
plot(y(1,:),y(2,:));

% unscented
D = cholcov(ysigmaUnscented([dim1,dim2],[dim1,dim2]));
y = ymiuUnscented([dim1,dim2]) + D*x;
plot(y(1,:),y(2,:));

% spherical simplex
D = cholcov(ysigmaSpherical([dim1,dim2],[dim1,dim2]));
y = ymiuSpherical([dim1,dim2]) + D*x;
plot(y(1,:),y(2,:));

% gauss-hermite
D = cholcov(ysigmaHermite([dim1,dim2],[dim1,dim2]));
y = ymiuHermite([dim1,dim2]) + D*x;
plot(y(1,:),y(2,:));

% monte carlo
D = cholcov(ysigmaMonteCarlo([dim1,dim2],[dim1,dim2]));
y = ymiuMonteCarlo([dim1,dim2]) + D*x;
plot(y(1,:),y(2,:));

legend('samples','Extended1','Extended2','Unscented','Spherical','Hermite','Monte Carlo');

end


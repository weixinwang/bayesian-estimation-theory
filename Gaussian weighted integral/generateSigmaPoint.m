function [w, x] = generateSigmaPoint( miu, sigma, method, aux )
% Generate sigma points and their corresponding weights
% miu: mean vector of input Gaussian distribution, nx-by-1 vector
% sigma: covariance matrix, nx-by-nx matrix
% method: 'Unscented': aux = w0, weight of origin
%         'Spherical': aux = w0, weight of origin
%         'Hermite': 
%         'Monte carlo': aux = N, number of monte carlo samples

% number of dimensions
nx = length(miu);

% cholesky decomposition of covariance matrix
[D,num] = cholcov(sigma);
if num > 0 || isempty(D)
    error('Covariance matrix is not semi-positive definite');
end

%% Unscented
if strcmp(method,'Unscented')
    if ~exist('aux','var')
        w0 = 0.25;
    else
        w0 = aux;
    end
    
    c = zeros(nx,2*nx+1);
    c(:,2:nx+1) = eye(nx)*sqrt(nx/(1-w0));
    c(:,nx+2:2*nx+1) = -eye(nx)*sqrt(nx/(1-w0));
    x = miu+D*c;
    
    w(1) = w0;
    w(2:2*nx+1) = (1-w0)/(2*nx);
end

%% Spherical Simplex
if strcmp(method,'Spherical')
    if ~exist('aux','var')
        w0 = 0.25;
    else
        w0 = aux;
    end
    w1 = (1-w0)/2;
    
    c = zeros(nx,nx+2);
    c(1,2) = 1/sqrt(2*w1); c(1,3) = -1/sqrt(2*w1);
    for i = 2:nx
        c(1:i,2:i+1) = [c(1:i-1,2:i+1);-ones(1,i)/sqrt(i*(i+1)*w1)];
        c(1:i,i+2) = [c(1:i-1,1);i/sqrt(i*(i+1)*w1)];
    end
    x = miu+D*c;
    
    w(1) = w0;
    w(2:nx+2) = (1-w0)/(nx+1);
end

%% Gaussian Hermite
if strcmp(method,'Hermite')
    r = zeros(nx,3^nx);
    w = zeros(1,3^nx); w(1) = (2/3)^nx;
    
    for i = 1:nx
        % i is the number of non-zero entries
        nonZeroIndex = combnk(1:nx,i);
        indexBeginI = 2;
        for j = 1:i-1
            indexBeginI = indexBeginI + factorial(nx)/factorial(j)/factorial(nx-j)*2^(j);
        end
        for j = 1:size(nonZeroIndex,1)
            % j is the number of enumeration of combinations of the index
            % of non-zeros entries
            for k = 0:i
                % k is the number of ones in the i non-zero entries
                oneIndex = combnk(nonZeroIndex(j,:),k);
                indexBeginK = indexBeginI + (j-1)*2^i;
                for n = 0:k-1
                    indexBeginK = indexBeginK + factorial(i)/factorial(n)/factorial(i-n);
                end
                for n = 1:size(oneIndex,1)
                    % n the number of enumeration of combinations of the
                    % index of one entries
                    negativeOneIndex = setdiff(nonZeroIndex(j,:),oneIndex(n,:));
                    indexN = indexBeginK + n-1;
                    if (~isempty(oneIndex))
                        r(oneIndex(n,:),indexN) = 1;
                    end
                    if (~isempty(negativeOneIndex))
                        r(negativeOneIndex,indexN) = -1;
                    end
                    w(indexN) = (2/3)^(nx-i)*(1/6)^i;
                end
            end
        end
    end
    
    x = miu+sqrt(3)*D*r;
end

%% Monte Carlo
if strcmp(method,'Monte carlo')
    if ~exist('aux','var')
        if nx < 4
            n = 10^nx;
        else
            n = 10^4;
        end
    else
        n = aux;
    end
    
    x = mvnrnd(miu',sigma,n)';
    w = ones(1,n)/n;
end

%% Wrong parameter
if ~exist('w','var')
    error('Method is not "Unscented", "Spherical", "Hermite" or "Monte carlo"');
end

end


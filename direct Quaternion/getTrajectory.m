function [ position ] = getTrajectory(gyro,acce,IMU,obs,KF)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('IMU','var')
    IMU = 'MPU';
end
if ~exist('obs','var')
    obs = 'qua';
end
if strcmp(obs,'grav') && ~exist('KF','var')
    KF = 'EKF';
end
if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    addpath('..\Gaussian weighted integral');
end

% Constants
g = [0;0;-9.821];
grav = norm(g);
gyroThresholdVelocity = 0.8;
acceThreshold = 0.8;

% state variables
P = zeros(3,1);
V = zeros(3,1);
Q = getInitQuat(acce(1,:));

% kalman filter parameters
conf = getMeaConf();
PKF = conf.quaternion*eye(4);
if strcmp(IMU,'MPU')
    [dt,QG] = getIMU_MPU();
elseif strcmp(IMU,'YOS')
    [dt,QG] = getIMU_YOST();
elseif strcmp(IMU,'XSE')
    [dt,QG] = getIMU_XSENS();
elseif strcmp(IMU,'MEM')
    [dt,QG] = getIMU_MEM();
end
QKF = QG*eye(3);
if strcmp(obs,'qua')
    RKF = conf.quaternion^2*eye(4);
elseif strcmp(obs,'grav')
    RKF = conf.gravity^2*eye(3);
end

% Monitors
quaternion = zeros(size(gyro,1),4);
acceleration = zeros(size(gyro,1),3);
velocity = zeros(size(gyro,1),3);
position = zeros(size(gyro,1),3);

% pre-analysis
applyKFZUPT = sqrt(sum(gyro.^2,2))<gyroThresholdVelocity & abs(sqrt(sum(acce.^2,2))-grav)<acceThreshold;
addpath('..');
midStance = findMidStance(gyro,acce,dt,g);
rmpath('..');

obsQua = strcmp(obs,'qua');
for i = 2:1:size(gyro,1)
    % forward integration
    PKF = covProp(PKF,QKF,Q,gyro(i-1,:)',dt);
    [P,V,Q,A] = quatInt(acce(i,:)',gyro(i,:)',acce(i-1,:)',gyro(i-1,:)',P,V,Q,dt,g);
    
    % ESKF
    if applyKFZUPT(i)
        if obsQua
            E = qua2eul(Q);
            EG = qua2eul(getInitQuat(acce(i,:)));
            QG = eul2qua([EG(1),EG(2),E(3)])';
            if sqrt(sum((QG-Q).^2)) > 0.5
                Error = -QG-Q;
            else
                Error = QG-Q;
            end
            K = PKF*(RKF+PKF)^-1;
            Q = Q+K*Error;
            Q = Q/norm(Q);
            PKF = (eye(4)-K)*PKF;
        else
            if strcmp(KF,'EKF')
                Error = acce(i,:)'./norm(acce(i,:))-QuatToRM(Q)'*[0;0;1];
                H = 2*[-Q(3), Q(4), -Q(1), Q(2)
                    Q(2), Q(1), Q(4), Q(3)
                    Q(1), -Q(2), -Q(3), Q(4)];
                K = PKF*H'*(RKF+H*PKF*H')^-1;
                Q = Q+K*Error;
                Q = Q/norm(Q);
                PKF = (eye(4)-K*H)*PKF;
            elseif strcmp(KF,'UKF')
                [w,sig] = generateSigmaPoint(Q,PKF,'Unscented');
                gravSig = zeros(3,length(w));
                Pzz = zeros(3); Pxz = zeros(4,3);
                for j = 1:length(w)
                    gravSig(:,j) = [2*sig(2,j)*sig(4,j)-2*sig(1,j)*sig(3,j); 2*sig(1,j)*sig(2,j)+2*sig(3,j)*sig(4,j); sig(1,j)^2-sig(2,j)^2-sig(3,j)^2+sig(4,j)^2];
                    Pzz = Pzz+w(j)*gravSig(:,j)*gravSig(:,j)';
                    Pxz = Pxz+w(j)*sig(:,j)*gravSig(:,j)';
                end
                QG = gravSig*w';
                Pzz = Pzz-QG*QG'+RKF;
                Pxz = Pxz-Q*QG';
                K = Pxz*Pzz^-1;
                Q = Q + K*(acce(i,:)'./norm(acce(i,:))-QG);
                PKF = PKF-K*Pzz*K';
            end
        end
    end
    
    % velocity correction
    k = find(midStance == i,1);
    if ~isempty(k)
        if k > 1
            prei = midStance(k-1);
            EA = V/(dt*(i-prei));
            for j = prei+1:i
                position(j,:) = position(j,:)-0.5*EA'*(dt*(j-prei))^2;
                velocity(j,:) = velocity(j,:)-EA'*(dt*(j-prei));
            end
            P = P - 0.5*EA*(dt*(i-prei))^2;
        end
        V = [0;0;0];
    end
    
    quaternion(i,:) = Q;
    acceleration(i,:) = A;
    velocity(i,:) = V;
    position(i,:) = P;
end

figure;
plot3(position(:,1),position(:,2),position(:,3));
axis equal; grid on;

if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    rmpath('..\Gaussian weighted integral');
end

end


function [ result ] = getTrajectory(gyro,acce,IMU,obs,KF)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('IMU','var')
    IMU = 'MPU';
end
if ~exist('obs','var')
    obs = 'axis';
end
if strcmp(obs,'grav') && ~exist('KF','var')
    KF = 'EKF';
end
if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    addpath('..\Gaussian weighted integral');
end

% Constants
g = [0;0;-9.821];
grav = norm(g);
gyroThresholdVelocity = 0.8;
acceThreshold = 0.8;

% state variables
P = zeros(3,1);
V = zeros(3,1);
Q = getInitQuat(acce(1,:));

% kalman filter parameters
conf = getMeaConf();
PKF = conf.rotationVector^2*eye(3);
if strcmp(IMU,'MPU')
    [dt,QG] = getIMU_MPU();
elseif strcmp(IMU,'YOS')
    [dt,QG] = getIMU_YOST();
elseif strcmp(IMU,'XSE')
    [dt,QG] = getIMU_XSENS();
elseif strcmp(IMU,'MEM')
    [dt,QG] = getIMU_MEM();
end
QKF = QG*eye(3);
if strcmp(obs,'grav')
    RKF = conf.gravity^2*eye(3);
elseif strcmp(obs,'axis')
    RKF = conf.rotationVector^2*eye(3);
end

% Monitors
quaternion = zeros(size(gyro,1),4);
acceleration = zeros(size(gyro,1),3);
velocity = zeros(size(gyro,1),3);
position = zeros(size(gyro,1),3);
theta = zeros(size(gyro,1),3);
postCM = zeros(3,3,size(gyro,1));

% pre-analysis
applyKFZUPT = sqrt(sum(gyro.^2,2))<gyroThresholdVelocity & abs(sqrt(sum(acce.^2,2))-grav)<acceThreshold;
addpath('..');
midStance = findMidStance(gyro,acce,dt,g);
rmpath('..');

obsAxis = strcmp(obs,'axis');
for i = 2:1:size(gyro,1)
    % forward integration
    [P,V,Q,A] = quatInt(acce(i,:)',gyro(i,:)',acce(i-1,:)',gyro(i-1,:)',P,V,Q,dt,g);
    
    % ESKF
    PKF = PKF+QKF;
    if applyKFZUPT(i)
        if obsAxis     % convert Grav to axis angle as observation
            EG = qua2eul(getInitQuat(acce(i,:)));
            E = qua2eul(Q);
            QG = eul2qua([EG(1:2),E(3)])';
            if norm(QG-Q) > 0.5
                QG = -QG;
            end
            Error = logQuat(QuatMultiply(QG,invQuat(Q)))';
            K = PKF*(PKF+RKF)^-1;
            Q = QuatMultiply(expQuat(K*Error),Q);
            PKF = (eye(3)-K)*PKF;
            
            theta(i,:) = (K*Error)';
        else          % observe gravity directly
            GQ = QuatToRM(Q)'*[0;0;1];
            Error = acce(i,:)'./norm(acce(i,:))-GQ;
            H = [2*Q(2)*Q(3)+2*Q(1)*Q(4), Q(3)^2+Q(4)^2-Q(1)^2-Q(2)^2, 0
                 -Q(2)^2+Q(1)^2-Q(4)^2+Q(3)^2, -2*Q(2)*Q(3)+2*Q(1)*Q(4), 0
                 -2*Q(1)*Q(2)+2*Q(3)*Q(4), -2*Q(1)*Q(3)-2*Q(2)*Q(4), 0];
            K = PKF*H'*(H*PKF*H'+RKF)^-1;
            Q = QuatMultiply(expQuat(K*Error),Q);
            PKF = (eye(3)-K*H)*PKF;
        end
    end
    
    % velocity correction
    k = find(midStance == i,1);
    if ~isempty(k)
        if k > 1
            prei = midStance(k-1);
            EA = V/(dt*(i-prei));
            for j = prei+1:i
                position(j,:) = position(j,:)-0.5*EA'*(dt*(j-prei))^2;
                velocity(j,:) = velocity(j,:)-EA'*(dt*(j-prei));
            end
            P = P - 0.5*EA*(dt*(i-prei))^2;
        end
        V = [0;0;0];
    end
    
    quaternion(i,:) = Q;
    acceleration(i,:) = A;
    velocity(i,:) = V;
    position(i,:) = P;
    postCM(:,:,i) = PKF;
end

figure;
plot3(position(:,1),position(:,2),position(:,3));
axis equal; grid on;

result.E = qua2eul(quaternion);
result.A = acceleration;
result.V = velocity;
result.P = position;
result.ZUPT = applyKFZUPT;
result.THETA = theta;
result.COV = postCM;

if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    rmpath('..\Gaussian weighted integral');
end

end


function [ Q0 ] = getInitQuat( acce )

u = cross([0;0;1],acce');

st = sqrt(sum(u.^2))/sqrt(sum(acce.^2));
ct = acce(3)/sqrt(sum(acce.^2));
theta = atan2(st,ct);

u = u/sqrt(sum(u.^2));

Q0 = expQuat(-u*theta);

end
function [midStance] = findMidStance( gyro, acce, dt, g )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% find healStrike and toeOff
grav = norm(g);
gyroThreshold = 1.0;
acceThreshold = 0.8;
shortHighThreshold = 5;
shortLowThreshold = 10;

lowMotion = sqrt(sum(gyro.^2,2))<gyroThreshold & abs(sqrt(sum(acce.^2,2))-grav)<acceThreshold;
lowMotionStart = find(diff(lowMotion) == 1)+1;
lowMotionEnd = find(diff(lowMotion) == -1);
if lowMotion(1)
    lowMotionStart = [1;lowMotionStart];
end
if lowMotion(end)
    lowMotionEnd = [lowMotionEnd;length(gyro)];
end

highMotionLength = lowMotionStart(2:end)-lowMotionEnd(1:end-1);
lowMotionStart(find(highMotionLength <= shortHighThreshold)+1) = [];
lowMotionEnd(highMotionLength <= shortHighThreshold) = [];

lowMotionLength = lowMotionEnd-lowMotionStart;
lowMotionStart(lowMotionLength <= shortLowThreshold) = [];
lowMotionEnd(lowMotionLength <= shortLowThreshold) = [];

stance = zeros(length(gyro),1);
for i = 1:length(lowMotionStart)
    stance(lowMotionStart(i):lowMotionEnd(i)) = 1;
end

% find midStance
num = 0;
midStance = zeros(length(gyro),1);
longStanceThreshold = 1/dt;
for i = 1:length(lowMotionStart)
    if lowMotionEnd(i)-lowMotionStart(i) > longStanceThreshold
        for j = lowMotionStart(i):round(1/dt):lowMotionEnd(i)
            num = num+1;
            index = j:min(j+round(1/dt)-1,lowMotionEnd(i));
            [aux,k] = min(sqrt(sum(gyro(index,:).^2,2)));
            midStance(num) = k+j-1;
        end
    else
        num = num+1;
        index = lowMotionStart(i):lowMotionEnd(i);
        [aux,k] = min(sqrt(sum(gyro(index,:).^2,2)));
        midStance(num) = k+lowMotionStart(i)-1;
    end
end
midStance(num+1:end) = [];

% figure; hold on;
% plot(gyro);
% plot(stance);
% ax = gca; ax.XTick = midStance;

end


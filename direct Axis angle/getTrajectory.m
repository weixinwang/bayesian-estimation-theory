function [ position ] = getTrajectory(gyro,acce,IMU,obs,KF)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('IMU','var')
    IMU = 'MPU';
end
if ~exist('obs','var')
    obs = 'axis';
end
if strcmp(obs,'grav') && ~exist('KF','var')
    KF = 'EKF';
end
if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    addpath('..\Gaussian weighted integral');
end

% Constants
g = [0;0;-9.821];
grav = norm(g);
gyroThresholdVelocity = 0.8;
acceThreshold = 0.8;

% state variables
P = zeros(3,1);
V = zeros(3,1);
RV = zeros(3,1);

% kalman filter parameters
conf = getMeaConf();
PKF = conf.rotationVector^2*eye(3);
if strcmp(IMU,'MPU')
    [dt,QG] = getIMU_MPU();
elseif strcmp(IMU,'YOS')
    [dt,QG] = getIMU_YOST();
elseif strcmp(IMU,'XSE')
    [dt,QG] = getIMU_XSENS();
elseif strcmp(IMU,'MEM')
    [dt,QG] = getIMU_MEM();
end
QKF = QG*eye(3);
if strcmp(obs,'grav')
    RKF = conf.gravity^2*eye(3);
elseif strcmp(obs,'axis')
    RKF = conf.rotationVector^2*eye(3);
end

% Monitors
rotateVector = zeros(size(gyro,1),3);
acceleration = zeros(size(gyro,1),3);
velocity = zeros(size(gyro,1),3);
position = zeros(size(gyro,1),3);

% pre-analysis
applyKFZUPT = sqrt(sum(gyro.^2,2))<gyroThresholdVelocity & abs(sqrt(sum(acce.^2,2))-grav)<acceThreshold;
addpath('..');
midStance = findMidStance(gyro,acce,g,dt);
rmpath('..');

obsAxis = strcmp(obs,'axis');
for i = 2:1:size(gyro,1)
    if (i == 2)
        RV = logQuat(getInitQuat(acce(1,:)))';
    end
    
    % forward integration
    [P,V,RV,A] = vectInt(acce(i,:)',gyro(i,:)',acce(i-1,:)',gyro(i-1,:)',P,V,RV,dt,g);
    
    % ESKF
    PKF = PKF+QKF;
    if applyKFZUPT(i)
        if obsAxis     % convert Grav to axis angle as observation
            EG = qua2eul(getInitQuat(acce(i,:)))';
            E = qua2eul(expQuat(RV))';
            K = PKF/(PKF+RKF);
            E = [E(1:2)+K(1:2,1:2)*(EG(1:2)-E(1:2));E(3)];
            RV = logQuat(eul2qua(E));
            PKF = (eye(3)-K)*PKF;
        else          % observe gravity directly
            GQ = QuatToRM(RV)'*[0;0;1];
            Error = acce(i,:)'./norm(acce(i,:))-GQ;
            H = [2*RV(2)*RV(3)+2*RV(1)*RV(4), RV(3)^2+RV(4)^2-RV(1)^2-RV(2)^2, 0
                 -RV(2)^2+RV(1)^2+RV(4)^2-RV(3)^2, -2*RV(2)*RV(3)+2*RV(1)*RV(4), 0
                 -2*RV(1)*RV(2)+2*RV(3)*RV(4), -2*RV(1)*RV(3)-2*RV(2)*RV(4), 0];
            K = PKF*H'*(H*PKF*H'+RKF)^-1;
            RV = QuatMultiply(expQuat(K*Error),RV);
            PKF = (eye(3)-K*H)*PKF;
        end
    end
    
    % velocity correction
    k = find(midStance == i,1);
    if ~isempty(k)
        if k > 1
            prei = midStance(k-1);
            EA = V/(dt*(i-prei));
            for j = prei+1:i
                position(j,:) = position(j,:)-0.5*EA'*(dt*(j-prei))^2;
                velocity(j,:) = velocity(j,:)-EA'*(dt*(j-prei));
            end
            P = P - 0.5*EA*(dt*(i-prei))^2;
        end
        V = [0;0;0];
    end
    
    rotateVector(i,:) = RV;
    acceleration(i,:) = A;
    velocity(i,:) = V;
    position(i,:) = P;
end

figure;
plot3(position(:,1),position(:,2),position(:,3));
axis equal; grid on;

if strcmp(obs,'grav') && ~strcmp(KF,'EKF')
    rmpath('..\Gaussian weighted integral');
end

end


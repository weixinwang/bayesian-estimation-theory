function [ PKF ] = covProp( PKF, QKF, Q, gyro, dt )

G = 0.5*[-Q(2) Q(3) Q(4)
         Q(1) -Q(4) Q(3)
         Q(4) Q(1) -Q(2)
         -Q(3) Q(2) Q(1)];
omega = [0 0 0 0
         0 0 gyro(3) -gyro(2)
         0 -gyro(3) 0, gyro(1)
         0 gyro(2) -gyro(1), 0]*dt;
F = expm(omega);

PKF = F*PKF*F'+G*QKF*G';

end

